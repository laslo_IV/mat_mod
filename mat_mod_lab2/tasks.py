

def feedback(N, D, k):
    Df = [D[i] + k*N[i] for i in range(len(D))]
    return Df

def transpose(A):		    #Just transposing a matrix of any size
	s = len(A)
	B = [ [ 0 for x in range(0,s) ] for x in range(0,s)]

	for i in range(0,s): 
		for j in range(0,s):  
			B[i][j] = A [j][i]
	return B;
 
def clmn2vctr(B):  			#Converting a column into a vector
	s = len(B)
	C = [[0 for x in range(0,s)]]
	for i in range(0,s):
		C[0][i] = B[i][0]
	return C;

def vctr2clmn(C):			#Converting a vector inti a column
	s = len(C[0])
	B = [ [0] for x in range(0,s) ]
	for i in range(0,s):
		B[i][0] = C[0][i]
	return B;

def kfSwap(KFN):			#Converting a "KFN" into "KNU" and vise-verse
	A1 = transpose(KFN[0])
	C1 = clmn2vctr(KFN[1])
	B1 = vctr2clmn(KFN[2])
	KFU = [A1,B1,C1,KFN[3]]
	return KFU;
