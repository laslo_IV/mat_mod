from scipy import signal
import matplotlib.pyplot as plt
from tasks import *


N = [ 1.0, 0.0, 3.0, 0.0]
D = [ 7.0, 0.0, 2.0, 1.0]


c = signal.tf2zpk(N, D)
print()
for i in c[0]:
    print("нули передаточной ф-и : " + str(i))
print()
for i in c[1]:
    print("полюса передаточной ф-и : " + str(i))
print()
print("коеф усиления передаточной ф-и : " + str(c[2]))
print()


Df = feedback(N, D, 10)
system = (N, Df)
imp = signal.impulse(system)
ste = signal.step(system)
plt.subplot(2,1,1)
plt.plot(imp[0],imp[1])
plt.subplot(2,1,2)
plt.plot(ste[0],ste[1])
plt.show()

KFN = signal.tf2ss(N,Df) 	#Converting N,D to A,B,C,D
KFUss = kfSwap(KFN) #Converting KFN to KFU
KFU = signal.ss2tf(KFUss[0],KFUss[1],KFUss[2],KFUss[3]) #Converting from A,B,C,D to N,D



Ikfu = signal.dimpulse((KFU[0],KFU[1],0.1))
Iraw = signal.dimpulse((N,Df,0.1))

Sraw = signal.step((N,Df))
Skfu = signal.step((KFU[0],KFU[1]))

I = [ (Iraw[1][0][i] - Ikfu[1][0][i]) for i in range(0,100)]
S = [ (Sraw[1][i] - Skfu[1][i]) for i in range (0,100)]


plt.subplot(2,1,1)
plt.plot(Iraw[0],I)
plt.subplot(2,1,2)
plt.plot(Sraw[0],S)
plt.show()

