from numpy import arange, arccos, cos
from math import pi, floor, ceil, sqrt, factorial
from matplotlib.pyplot import subplot, plot, grid, show, axis, gca
from sympy import symbols, I, re, im
import random


def binomial_cooficient(n, m):
    return factorial(n) / (factorial(m) * factorial(n - m))


def lissajous_figure(w, d_phi=4, A=[1.0, 1.0]):
    color = ['b', 'g']          # blue/green
    nrows = ceil(sqrt(d_phi))   # number of rows
    ncols = floor(sqrt(d_phi))  # number of colomns
    schedule = [[nrows, ncols, j] for j in range(1, d_phi + 1)]  # number of rows/number of colomns/index number

    phi = [i for i in arange(0., pi, pi / d_phi)]               #angle
    x = [A[0] * cos(w[0] * t) for t in arange(0, 10 * pi, 0.01)] #x=A*cos(wt)
    for i in range(0, d_phi):
        y = [A[1] * cos(w[1] * t + phi[i]) for t in arange(0, 10 * pi, 0.01)]#y=A*cos(wt+phi)
        subplot(schedule[i][0], schedule[i][1], schedule[i][2])             #dividing the graphics
        grid(True)                                                          #setting the grid
        plot(x, y, color[i % len(color)])                                   #drawing
    show()                                                                  #showing


def cheb_poly_subfoo(n, x):
    a = 0
    for j in range(floor(n / 2)):
        a += binomial_cooficient(n, 2 * j) * (x ** 2 - 1) ** j * x ** (n - 2 * j)
    return a


def chebishev_polinom_2(n):
    x = [i for i in arange(-1, 1, 0.01)]
    y = [cheb_poly_subfoo(n, i) for i in arange(-1, 1, 0.01)]
    grid(True)
    plot(x, y, "b")
    show()


def chebishev_polinom_1(n):
    x = [i for i in arange(-1, 1, 0.01)]
    y = [cos(n * arccos(i)) for i in arange(-1, 1, 0.01)]
    grid(True)
    plot(x, y, "b")
    show()


from sympy import symbols, I, re, im
def godograph_michailova():
    w = symbols(' w', real=True)
    D = w ** 8 - 3 * w ** 6 - 8 * I * w ** 3 + 2 * I * w + 1
    Re = re(D)
    Im = im(D)
    x = [Re.subs({w: i}) for i in arange(0, 100, 0.1)]
    y = [Im.subs({w: i}) for i in arange(0, 100, 0.1)]
    axis([-10.0, 10.0, -50.0, 50.0])
    ax = gca()
    ax.spines['left'].set_position('center')
    ax.spines['bottom'].set_position('center')
    ax.spines['top'].set_visible(False)
    ax.spines['right'].set_visible(False)
    plot(x, y, "b")
    grid(True)
    show()

def ATM():
    moneyInATM = random.random() * 10000
    print("Enter card:")
    card=str(input())
    if(len(card)!=16):
        print("invalid card number")
        return
    print("Enter code:")
    code=str(input())
    if(len(code)!=4):
        print("Enter again")
        code=str(input())
        if(len(code)!=4):
            print("Invalid code.Your card will be blocked")
            return
    #some connection to database and getting the data of card
    clientMoney=random.random()*1000
    while(True):
        print("1. Стан рахунку")
        print("2. Зняти грощі")
        print("3. Вихід")
        number=int(input())
        if(number==1):
            print("Money: "+str("%.2f" % clientMoney))
            continue
        if(number==2):
            print("Sum:")
            sum=int(input())
            if(sum<=moneyInATM):
                moneyInATM-=sum
                clientMoney-=sum
                print("Take your money")
            else:
                print("Error: lack of money in ATM")
            continue
        if(number==3):
            print("Goodbye")
            break;

